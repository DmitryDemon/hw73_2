const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;

const password = 'siskymamystiflera';


app.get('/encode/:password', (req, res) => {
    const coded = Vigenere.Cipher(password).crypt(req.params.password);
    res.send(`Coded string: ${coded}`)
});


app.get('/decode/:password', (req, res) => {
    const decoded = Vigenere.Decipher(password).crypt(req.params.password);
    res.send(`Decoded string: ${decoded}`)
});

app.listen(port, () => {
    console.log('Сервер поднят блеАт!!! НЕ жуй мозги!!!! ' + port);
});